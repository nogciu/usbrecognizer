#include <stdio.h>
#include <iostream>
#include <vector>


const std::string pololuVendor = "FTDI";
const std::string arduinoVendor = "Arduino_Srl";

int main()
{

    std::string pololuDevice;
    std::string arduinoDevice;
    std::vector < std::string > devices;
    char c;
    std::string word;
    FILE *fp = popen("ls /dev | grep ttyUSB", "r");

    while (c != EOF)
    {
        c = fgetc(fp);
        if(isspace(c))
        {
            devices.push_back(word);
            word.clear();
        }
        else if (c!=EOF)
        {
            word += c;

        } else if (word.empty() == false)
        {
            devices.push_back(word);
            break;
        }
    }

    pclose(fp);

    for(size_t i = 0; i < devices.size(); i++)
    {
        std::cout << devices.at(i) << std::endl;
        std::string command;
        command = "udevadm info --query=all /dev/" + devices.at(i) + " | grep 'ID_VENDOR' | head -1";
        fp = popen(command.c_str(), "r");

        std::string word2;

        char buffer[1024];

        char *line_p = fgets(buffer, sizeof(buffer), fp);

        for (size_t i = 0; i < sizeof(buffer); i++)
        {
            if(buffer[i] == '=')
            {
                i++;
                while(buffer[i] != '\n' && i < sizeof(buffer))
                {

                    word2.push_back(buffer[i]);
                    i++;
                }
                break;
            }
        }


        std::cout << word2 << std::endl;

        if(word2 == pololuVendor)
            pololuDevice = "/dev/" + devices.at(i);

        else if(word2 == arduinoVendor)
            arduinoDevice = "/dev/" + devices.at(i);
        pclose(fp);


    }


    std::cout << "pololu = " << pololuDevice << std::endl;
    std::cout << "arduino = " << arduinoDevice << std::endl;

    return 0;

}
